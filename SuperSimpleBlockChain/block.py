""" block.py

"""
# /usr/bin/python
# -*- coding=utf-8 -*-

import hashlib
from datetime import datetime

class Block(object):
    """ Block structure.

    """
    def __init__(self, index, previous_hash, data):
        """ Init.

        """
        self.index = index
        self.previous_hash = previous_hash
        self.timestamp = str(datetime.now())
        self.data = data
        self.hash = self.__calc_hash_myself()

    def __calc_hash_myself(self):
        """ Calculate sha256 hash code method.

        """
        p_h = str(self.previous_hash)
        data = str(self.index) + p_h + self.timestamp + self.data
        # data = str(self.index) + self.previous_hash.decode("utf-8") + self.timestamp + self.data
        h = hashlib.sha256()
        h.update(data.encode())
        return str(h.digest())

class PreviousBlock(object):
    """ Previous Block Structure. For hash check.

    """
    def __init__(self, index, hash):
        """ Init.

        """
        self.index = index
        self.hash = hash
