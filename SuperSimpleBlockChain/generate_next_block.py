# /usr/bin/python
# -*- coding=utf-8 -*-

from block import Block
from datetime import datetime

def generate_next_block(previous_block, data):
    """ Generate next block.

    """
    next_index = int(previous_block.index) + 1
    time_stamp = str(datetime.now())
    return Block(next_index, previous_block.hash, data)
