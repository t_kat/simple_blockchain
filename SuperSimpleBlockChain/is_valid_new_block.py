""" is_valid_new_block.py

"""
# /usr/bin/python
# -*- coding=utf-8 -*-
from generate_next_block import generate_next_block

def is_valid_new_block(new_block, previous_block):
    """ Validate new block.

    """
    # index check.
    if new_block.index != (previous_block.index + 1):
        return False

    # hash check.
    if new_block.previous_hash != previous_block.hash:
        return False

    return True
