""" simple_blockchain.py

"""
# /usr/bin/python
# -*- coding=utf-8 -*-
import block
import genesis_block
from generate_next_block import generate_next_block
from is_valid_new_block import is_valid_new_block
import database

def main(data):
    """ Main method.

    """
    # genesis block.
    if database.recode_count() == 0:
        gen_block = genesis_block.genesis_block()
        database.insert_block(gen_block)

    # previous block.
    previous_block = database.fetch_previous_block()
    previous_block = block.PreviousBlock(previous_block[0], previous_block[4])
    new_block = generate_next_block(previous_block, data)

    if is_valid_new_block(new_block, previous_block) == True:
        database.insert_block(new_block)

        print("Block{0} append OK!".format(new_block.index))
        print("block index, data: {0}, {1}".format(new_block.index, new_block.data))
        print("block hash: {0}".format(new_block.hash))
        print("previous hash: {0}".format(new_block.previous_hash))
        print("--------------------------------------------------------------")

if __name__ == "__main__":
    LOOP = True
    DATA = None
    while LOOP == True:
        DATA = str(input())
        if len(DATA) <= 512 and DATA != "q":
            main(DATA)
        else:
            print("exit.")
            database.close()
            LOOP = False
