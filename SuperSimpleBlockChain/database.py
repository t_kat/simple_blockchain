""" database.py

"""
# /usr/bin/python
# -*- coding=utf-8 -*-

import sqlite3

CONNECT = sqlite3.connect('./database/block_chain.db')
CURSOR = CONNECT.cursor()

QUERY = '''CREATE TABLE BLOCK_CHAIN (recode_index INTEGER PRIMARY KEY, previous_hash VARCHAR(256), timestamp VARCHAR(20), data VARCHAR(512), hash VARCHAR(256));'''

try:
    CURSOR.execute(QUERY)
except sqlite3.OperationalError:
    # table is.
    pass

def insert_block(block):
    """ insert block.

    """
    query = 'INSERT INTO BLOCK_CHAIN (recode_index, previous_hash, timestamp, data, hash) values(?,?,?,?,?)'
    try:
        CURSOR.execute(query, (block.index, block.previous_hash, block.timestamp, block.data, block.hash))
        CONNECT.commit()
    except sqlite3.Error:
        print('query is invalid.')
        CONNECT.rollback()
        CURSOR.close()
        CONNECT.close()

def fetch_previous_block():
    """ fetch_previous_block.

    """
    query = 'SELECT * FROM BLOCK_CHAIN ORDER BY RECODE_INDEX DESC;'
    try:
        CURSOR.execute(query)
    except sqlite3.Error:
        print('query is invalid.')
        CURSOR.close()
        CONNECT.close()

    return CURSOR.fetchall()[0]

def recode_count():
    """ recode_count.

    """
    query = 'SELECT * FROM BLOCK_CHAIN;'
    try:
        CURSOR.execute(query)
    except sqlite3.Error:
        print('query is invalid.')
        CURSOR.close()
        CONNECT.close()

    return len(CURSOR.fetchall())

def close():
    """ close.

    """
    CURSOR.close()
    CONNECT.close()
