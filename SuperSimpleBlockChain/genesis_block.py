""" genesis_block.py

"""
# /usr/bin/python
# -*- coding=utf-8 -*-

from datetime import datetime
from block import Block

def genesis_block():
    """ Generate genesis block.

    """
    return Block(0, "0", "genesis block. block chain start!")
